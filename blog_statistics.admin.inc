<?php
/**
 * @file
 *   foo
 */

function blog_statistics_settings() {
  $form['rebuild'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild blog_statistics')
  );

  return $form;
}

function blog_statistics_settings_submit(&$form, &$form_state) {
  // get all group nodes
  $result = db_query("SELECT uid FROM {users}");
  while ($item = db_fetch_object($result)) {
    $users[$item->uid] = $item->uid;
  }
  $counter = 0;
  foreach ($users as $uid) {
    $counter ++;
    $array[] = $uid;
    if (($counter % 20) == 1) {
      $operations[] = array('blog_statistcs_recalc', array($array));
      $array = array();
    }
  }
  $operations[] = array('blog_statistcs_recalc', array($array));
  // Execute the batch progress
  $batch = array(
    'operations' => $operations,
    'title' => 'Generating statistics',
    'init_message' => 'Loading group nodes',
    'error_message' => 'An unrecoverable error has occurred. You can find the error message below. It is advised to copy it to the clipboard for reference.',
    'finished' => 'blog_statistcs_settings_finished',
  );
  batch_set($batch);
  batch_process('admin/settings/blog_statistics');
}
