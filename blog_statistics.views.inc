<?php

/**
 * @file
 *   Views Integration of og_statitics.
 */

/**
 * Implementation of hook_views_data().
 */
function blog_statistics_views_data() {
  $data = array();
  $data += blog_statistics_views_og_statistics();
  return $data;
}

function blog_statistics_views_og_statistics() {
  $data['blog_statistics']['table']['group'] = t('Blog Statistics');

  $data['blog_statistics']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['blog_statistics']['posts_count'] = array(
    'title' => t('Blog Statistic: Posts Count'),
    'real field' => 'posts_count',
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );
  $data['blog_statistics']['comments_count'] = array(
    'title' => t('Blog Statistic: Comments Count'),
    'real field' => 'comments_count',
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  // Add relationship to node table for last comment posted.
  $data['blog_statistics']['last_comment_nid'] = array(
    'title' => 'Blog Statistic: Node the last comment was posted to',
    'help' => 'Create a relationship to the last comment posted',
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'field' => 'nid',
      'label' => t('Last commented blog node'),
    ),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );

  // Add relationship to node table for last comment posted.
  $data['blog_statistics']['last_created_nid'] = array(
    'title' => 'Blog Statistic: Node which was created last',
    'help' => '',
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'field' => 'nid',
      'label' => t('Created blog node'),
    ),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );

  // Add relationship to node table for last comment posted.
  $data['blog_statistics']['last_update_nid'] = array(
    'title' => 'Blog Statistic: Node which was updated last',
    'help' => '',
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'field' => 'nid',
      'label' => t('Updated blog node'),
    ),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );

  return $data;
}

